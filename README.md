# Gaze reading analyzer

Analiza skupienia uwagi użytkownika na podstawie badania wzroku - czyli badanie wpływu fiksacji na zrozumienie czytanego tekstu/modeli. 

## Opis projektu

### Analiza istniejącego modelu
![Analyze DMN model](https://i.imgur.com/kOXXyf2.png)

W zaznaczonym pomarańczowym prostokącie znajduje się przejście do tabelek z danymi modelu.

1. Przejście do ekranu z kalibracją. Przed rozpocząciem analizy modelu należy przejść przez proces kalibracji. Pozwala ona na zbieranie dokładniejszych danych przez eye tracker'a.
2. Włączenie śledzenie wzroku. Po naciśnięciu tego przycisku należy zezwolić przeglądarce na korzystanie z kamery. Po obrysowaniu zieloną linią rysów twarzy eye tracker rozpoczyna poprawne działania.
3. Włączenie pauzy w działaniu eye trackera.
4. Pobranie pliku csv z wynikami pobranymi podczas śledzenia wzorku.
5. Drukowanie modelu na konsolę. (Po naciśnięciu tego przycisku w narzędziach developerskich w zakładce Console powinna znaleźć się zawartość modelu).



### Analiza nowego utworzonego modelu
![Create new DMN model](https://i.imgur.com/9oBLMBK.png)

Aplikacja umożliwia również analizowanie modelu utworzonego przez użytkownika.
Tworzenie modelu odbywa się za pomocą panelu z narzędziami po lewej stronie.

### Kalibracja

![](https://i.imgur.com/8XXhvlT.png)

Proces kalibrowania eye trackera polega na śledzeniu myszki podczas klikania punktów na ekranie. Każdy punkt należy kliknąć 5 razy, aż punkt zmieni kolor na czarny. Gdy wszystkie punkty zmienią kolor na czarny, kalibracja się wyłącza. 

## Wykorzystane biblioteki


### Webgazer

[webgazer.js](https://webgazer.cs.brown.edu/)

- biblioteka javascriptowa, typu open-source do śledzenia wzroku. Korzysta z dostępnej na używanym urządzeniu kamery, 
- Używa modelu regresji do mapowania, współrzędnych oka do lokalizacji na ekranie,
- Detekcja odbywa się dla każdej ramki wideo,
- Przy każdorazowej interakcji użytkownika z aplikacją, WebGazer trenuje model regresji poprzez dopasowanie aktualnej lokalizacji współrzędnych na ekranie do pozycji oka,
- średni błąd: 100px,
- Przykład użycia: [A-Frame + WebGazer.JS for Immersive Desktop VR](https://www.youtube.com/watch?v=-7vhhY3Hsoc)

