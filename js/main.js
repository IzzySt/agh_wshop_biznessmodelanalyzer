let startTime;
let csv = "time,x,y,trial,word\n";
let isReaderReady = false;

function runWebgazer() {
  //start the webgazer tracker
  webgazer
    .setRegression("ridge") /* currently must set regression and tracker */
    .setTracker("clmtrackr")
    .setGazeListener(function (data, clock) {
      //   console.log(data); /* data is an object containing an x and y key which are the x and y prediction coordinates (no bounds limiting) */
      //   console.log(clock); /* elapsed time in milliseconds since webgazer.begin() was called */
      if (data == null) return;
      let modelTableEl = document.elementFromPoint(data.x, data.y);
      let word = "";
      if (modelTableEl != null) {
        word = this.getPickedRectLabel(modelTableEl);
        console.log(word);
      }
      //onsole.log("x: " + data.x + ", y: " + data.y + ", words: " + word);
      if (isReaderReady) {
        let dataRow = [Date.now() - startTime, data.x, data.y, 1, word];
        csv += dataRow.join(",");
        csv += "\n";
      }
    })
    .begin()
    .showPredictionPoints(false);

  //Set up the webgazer video feedback.
  var setup = function () {
    //Set up the main canvas. The main canvas is used to calibrate the webgazer.
    var canvas = document.getElementById("plotting_canvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvas.style.position = "fixed";
  };

  function checkIfReady() {
    if (webgazer.isReady()) {
      setup();
    } else {
      setTimeout(checkIfReady, 100);
    }
  }
  setTimeout(checkIfReady, 100);
}

function pauseWebgazer() {
  webgazer.pause();
  webgazer.stopVideo();
}

function getPickedRectLabel(modelTableEl) {
  if (modelTableEl.classList.contains("djs-hit")) {
    let rectContent = modelTableEl.previousElementSibling.lastElementChild;
    if (rectContent == null) return;
    return rectContent.textContent;
  }
}

window.onbeforeunload = function () {
  //webgazer.pause();
  //webgazer.end(); //Uncomment if you want to save the data even if you reload the page.
  window.localStorage.clear(); //Comment out if you want to save data across different sessions
};

function startReading() {
  startTime = Date.now();
  isReaderReady = true;
}

function finishWebGazerAndSavePredic() {
  webgazer.pause();

  var hiddenElement = document.createElement("a");
  hiddenElement.href = "data:text/csv;charset=utf-8," + encodeURI(csv);
  hiddenElement.target = "_blank";
  hiddenElement.download = "dataDemo.csv";
  hiddenElement.click();
}

function enableCalibrationMode() {
  $("#container").hide();
  $("#canvas").show();
  runWebgazer();
}

function disableCalibrationMode() {
  $("#canvas").hide();
  $("#container").show();
  startTime = Date.now();
  isReaderReady = true;
}
