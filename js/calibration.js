let points = [];
let pointColor = "230";
let h = 20;
let calibrationFinished = false;
function setup() {
  //Start();
  let canvas = createCanvas(windowWidth, windowHeight);
  canvas.parent("canvas");
  //top points
  points.push(new Point(windowWidth / 2, 2 * h, h, pointColor));
  points.push(new Point(windowWidth - 2 * h, 2 * h, h, pointColor));
  points.push(new Point(2 * h, 2 * h, h, pointColor));
  points.push(new Point(windowWidth / 2, windowHeight - 2 * h, h, pointColor));
  points.push(new Point(windowWidth - 2 * h, windowHeight - 2 * h, h, pointColor));
  //bottom points
  points.push(new Point(2 * h, windowHeight / 2, h, pointColor));
  points.push(new Point(windowWidth - 2 * h, windowHeight / 2, h, pointColor));
  points.push(new Point(windowWidth / 2, windowHeight / 2, h, pointColor));
}

function draw() {
  background("#fae7dc");
  let counter = 0;
  points.forEach(function (point) {
    point.show();
    if (point.isFinished() == true) {
      ++counter;
    }
  });

  finishCalibration(counter);
}

function mousePressed() {
  points.forEach(function (point) {
    point.click(mouseX, mouseY);
  });
}

function showLastCalibrationPoint() {
  point.show();
  point.click(mouseX, mouseY);

  if (point.isFinished()) {
    finishCalibration();
  }
}

function finishCalibration(counter) {
  if (counter == points.length && calibrationFinished == false) {
    //PopUpCalibrationFinished();
    disableCalibrationMode();
    calibrationFinished = true;
    noLoop();
  }
}
