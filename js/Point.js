class Point {
  constructor(x, y, h, color) {
    this.x = x;
    this.y = y;
    this.h = h;
    this.color = color;
  }

  changeColor(diff) {
    this.color -= diff;
  }

  show() {
    fill(this.color);
    ellipse(this.x, this.y, this.h);
  }

  getX() {
    return this.x;
  }

  getY() {
    return this.y;
  }

  getColor() {
    return this.color;
  }

  isFinished() {
    if (this.color <= 0) return true;
    return false;
  }

  click(distX, distY) {
    let distance = dist(distX, distY, this.x, this.y);
    if (distance < this.h) {
      this.changeColor(50);
    }
  }
}
